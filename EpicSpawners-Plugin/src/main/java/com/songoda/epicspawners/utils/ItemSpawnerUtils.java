package com.songoda.epicspawners.utils;

import java.lang.reflect.Method;

import org.bukkit.block.CreatureSpawner;
import org.bukkit.inventory.ItemStack;

import com.songoda.epicspawners.EpicSpawnersPlugin;

public class ItemSpawnerUtils {

	public static Class<?> WorldClass;
	public static Class<?> CraftWorldClass;
	public static Class<?> TileEntityClass;
	public static Class<?> BlockPositionClass;
	public static Class<?> TileEntityMobSpawnerClass;
	public static Class<?> NBTTagCompoundClass;
	public static Class<?> NBTBaseClass;
	public static Class<?> ItemStackClass;
	public static Class<?> CraftItemStackClass;

	public static void initializeClasses() {
		try {
			WorldClass = Class.forName("net.minecraft.server." + EpicSpawnersPlugin.nmsVersion + ".World");
			CraftWorldClass = Class.forName("org.bukkit.craftbukkit." + EpicSpawnersPlugin.nmsVersion + ".CraftWorld");
			TileEntityClass = Class.forName("net.minecraft.server." + EpicSpawnersPlugin.nmsVersion + ".TileEntity");
			BlockPositionClass = Class.forName("net.minecraft.server." + EpicSpawnersPlugin.nmsVersion + ".BlockPosition");
			TileEntityMobSpawnerClass = Class.forName("net.minecraft.server." + EpicSpawnersPlugin.nmsVersion + ".TileEntityMobSpawner");
			NBTTagCompoundClass = Class.forName("net.minecraft.server." + EpicSpawnersPlugin.nmsVersion + ".NBTTagCompound");
			NBTBaseClass = Class.forName("net.minecraft.server." + EpicSpawnersPlugin.nmsVersion + ".NBTBase");
			ItemStackClass = Class.forName("net.minecraft.server." + EpicSpawnersPlugin.nmsVersion + ".ItemStack");
			CraftItemStackClass = Class.forName("org.bukkit.craftbukkit." + EpicSpawnersPlugin.nmsVersion + ".inventory.CraftItemStack");
		} catch (Exception e) {
			Debugger.runReport(e);
		}
	}

	public static void changeSpawnerDisplayItem(CreatureSpawner spawner, ItemStack displayItem) {
		try {
			initializeClasses();

			if (EpicSpawnersPlugin.nmsVersion.startsWith("v1_8")) {
				Method getHandleMethodCraftWorld = CraftWorldClass.getDeclaredMethod("getHandle");
				Method getTileEntityMethodWorld = WorldClass.getDeclaredMethod("getTileEntity", BlockPositionClass);
				Method aMethodTileEntityMobSpawner = TileEntityMobSpawnerClass.getDeclaredMethod("a", NBTTagCompoundClass);
				Method bMethodTileEntityMobSpawner = TileEntityMobSpawnerClass.getDeclaredMethod("b", NBTTagCompoundClass);
				Method removeMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("remove", String.class);
				Method setStringMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("setString", String.class, String.class);
				Method setShortMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("setShort", String.class, short.class);
				Method setByteMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("setByte", String.class, byte.class);
				Method setMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("set", String.class, NBTBaseClass);
				Method asNMSCopyCraftItemStack = CraftItemStackClass.getDeclaredMethod("asNMSCopy", ItemStack.class);
				Method saveItemStack = ItemStackClass.getDeclaredMethod("save", NBTTagCompoundClass);

				Object craftWorld = CraftWorldClass.cast(spawner.getWorld());
				Object world = getHandleMethodCraftWorld.invoke(craftWorld);
				Object blockPosition = BlockPositionClass.getConstructor(new Class<?>[]{int.class, int.class, int.class}).newInstance(spawner.getX(), spawner.getY(), spawner.getZ());
				Object tileEntity = getTileEntityMethodWorld.invoke(world, blockPosition);
				Object mobSpawner = TileEntityMobSpawnerClass.cast(tileEntity);
				
				Object spawnerTag = NBTTagCompoundClass.newInstance();
				bMethodTileEntityMobSpawner.invoke(mobSpawner, spawnerTag);
				removeMethodNBTTagCompound.invoke(spawnerTag, "SpawnPotentials");
				setStringMethodNBTTagCompound.invoke(spawnerTag, "EntityId", "Item");

				Object itemTag = NBTTagCompoundClass.newInstance();
				setShortMethodNBTTagCompound.invoke(itemTag, "Health", (short) 5);
				setShortMethodNBTTagCompound.invoke(itemTag, "Age", (short) 0);

				Object handleitemStack = asNMSCopyCraftItemStack.invoke(CraftItemStackClass.getClass(), displayItem);

				Object itemStackTag = NBTTagCompoundClass.newInstance();

				saveItemStack.invoke(handleitemStack, itemStackTag);
				setByteMethodNBTTagCompound.invoke(itemStackTag, "Count", (byte)1);
				setMethodNBTTagCompound.invoke(itemTag, "Item", itemStackTag);
				setMethodNBTTagCompound.invoke(spawnerTag, "SpawnData", itemTag);
				aMethodTileEntityMobSpawner.invoke(mobSpawner, spawnerTag);
			}else if (EpicSpawnersPlugin.nmsVersion.startsWith("v1_9") || EpicSpawnersPlugin.nmsVersion.startsWith("v1_10") || EpicSpawnersPlugin.nmsVersion.startsWith("v1_11")) {
				Method getHandleMethodCraftWorld = CraftWorldClass.getDeclaredMethod("getHandle");
				Method getTileEntityMethodWorld = WorldClass.getDeclaredMethod("getTileEntity", BlockPositionClass);
				Method aMethodTileEntityMobSpawner = TileEntityMobSpawnerClass.getDeclaredMethod("a", NBTTagCompoundClass);
				Method saveMethodTileEntityMobSpawner = TileEntityMobSpawnerClass.getDeclaredMethod("save", NBTTagCompoundClass);
				Method removeMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("remove", String.class);
				Method setStringMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("setString", String.class, String.class);
				Method setShortMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("setShort", String.class, short.class);
				Method setByteMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("setByte", String.class, byte.class);
				Method setMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("set", String.class, NBTBaseClass);
				Method asNMSCopyCraftItemStack = CraftItemStackClass.getDeclaredMethod("asNMSCopy", ItemStack.class);
				Method saveItemStack = ItemStackClass.getDeclaredMethod("save", NBTTagCompoundClass);

				Object craftWorld = CraftWorldClass.cast(spawner.getWorld());
				Object world = getHandleMethodCraftWorld.invoke(craftWorld);
				Object blockPosition = BlockPositionClass.getConstructor(new Class<?>[]{int.class, int.class, int.class}).newInstance(spawner.getX(), spawner.getY(), spawner.getZ());
				Object tileEntity = getTileEntityMethodWorld.invoke(world, blockPosition);
				Object mobSpawner = TileEntityMobSpawnerClass.cast(tileEntity);
				
				Object spawnerTag = NBTTagCompoundClass.newInstance();
				saveMethodTileEntityMobSpawner.invoke(mobSpawner, spawnerTag);
				removeMethodNBTTagCompound.invoke(spawnerTag, "SpawnPotentials");

				Object itemTag = NBTTagCompoundClass.newInstance();
				setShortMethodNBTTagCompound.invoke(itemTag, "Health", (short) 5);
				setShortMethodNBTTagCompound.invoke(itemTag, "Age", (short) 0);

				Object handleitemStack = asNMSCopyCraftItemStack.invoke(CraftItemStackClass.getClass(), displayItem);

				Object itemStackTag = NBTTagCompoundClass.newInstance();

				saveItemStack.invoke(handleitemStack, itemStackTag);
				setByteMethodNBTTagCompound.invoke(itemStackTag, "Count", (byte)1);
				setMethodNBTTagCompound.invoke(itemTag, "Item", itemStackTag);
				setStringMethodNBTTagCompound.invoke(itemTag, "id", "Item");
				setMethodNBTTagCompound.invoke(spawnerTag, "SpawnData", itemTag);
				aMethodTileEntityMobSpawner.invoke(mobSpawner, spawnerTag);
			}else if (EpicSpawnersPlugin.nmsVersion.startsWith("v1_12")) {
				Method getHandleMethodCraftWorld = CraftWorldClass.getDeclaredMethod("getHandle");
				Method getTileEntityMethodWorld = WorldClass.getDeclaredMethod("getTileEntity", BlockPositionClass);
				Method loadMethodTileEntityMobSpawner = TileEntityMobSpawnerClass.getDeclaredMethod("load", NBTTagCompoundClass);
				Method saveMethodTileEntityMobSpawner = TileEntityMobSpawnerClass.getDeclaredMethod("save", NBTTagCompoundClass);
				Method removeMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("remove", String.class);
				Method setStringMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("setString", String.class, String.class);
				Method setShortMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("setShort", String.class, short.class);
				Method setByteMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("setByte", String.class, byte.class);
				Method setMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("set", String.class, NBTBaseClass);
				Method asNMSCopyCraftItemStack = CraftItemStackClass.getDeclaredMethod("asNMSCopy", ItemStack.class);
				Method saveItemStack = ItemStackClass.getDeclaredMethod("save", NBTTagCompoundClass);

				Object craftWorld = CraftWorldClass.cast(spawner.getWorld());
				Object world = getHandleMethodCraftWorld.invoke(craftWorld);
				Object blockPosition = BlockPositionClass.getConstructor(new Class<?>[]{int.class, int.class, int.class}).newInstance(spawner.getX(), spawner.getY(), spawner.getZ());
				Object tileEntity = getTileEntityMethodWorld.invoke(world, blockPosition);
				Object mobSpawner = TileEntityMobSpawnerClass.cast(tileEntity);
				
				Object spawnerTag = NBTTagCompoundClass.newInstance();
				saveMethodTileEntityMobSpawner.invoke(mobSpawner, spawnerTag);
				removeMethodNBTTagCompound.invoke(spawnerTag, "SpawnPotentials");

				Object itemTag = NBTTagCompoundClass.newInstance();
				setShortMethodNBTTagCompound.invoke(itemTag, "Health", (short) 5);
				setShortMethodNBTTagCompound.invoke(itemTag, "Age", (short) 0);

				Object handleitemStack = asNMSCopyCraftItemStack.invoke(CraftItemStackClass.getClass(), displayItem);

				Object itemStackTag = NBTTagCompoundClass.newInstance();

				saveItemStack.invoke(handleitemStack, itemStackTag);
				setByteMethodNBTTagCompound.invoke(itemStackTag, "Count", (byte)1);
				setMethodNBTTagCompound.invoke(itemTag, "Item", itemStackTag);
				setStringMethodNBTTagCompound.invoke(itemTag, "id", "Item");
				setMethodNBTTagCompound.invoke(spawnerTag, "SpawnData", itemTag);
				loadMethodTileEntityMobSpawner.invoke(mobSpawner, spawnerTag);
			}else if (EpicSpawnersPlugin.nmsVersion.startsWith("v1_13")) {
				Method getHandleMethodCraftWorld = CraftWorldClass.getDeclaredMethod("getHandle");
				Method getTileEntityMethodWorld = WorldClass.getDeclaredMethod("getTileEntity", BlockPositionClass);
				Method loadMethodTileEntityMobSpawner = TileEntityMobSpawnerClass.getDeclaredMethod("load", NBTTagCompoundClass);
				Method saveMethodTileEntityMobSpawner = TileEntityMobSpawnerClass.getDeclaredMethod("save", NBTTagCompoundClass);
				Method removeMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("remove", String.class);
				Method setStringMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("setString", String.class, String.class);
				Method setShortMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("setShort", String.class, short.class);
				Method setByteMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("setByte", String.class, byte.class);
				Method setMethodNBTTagCompound = NBTTagCompoundClass.getDeclaredMethod("set", String.class, NBTBaseClass);
				Method asNMSCopyCraftItemStack = CraftItemStackClass.getDeclaredMethod("asNMSCopy", ItemStack.class);
				Method saveItemStack = ItemStackClass.getDeclaredMethod("save", NBTTagCompoundClass);

				Object craftWorld = CraftWorldClass.cast(spawner.getWorld());
				Object world = getHandleMethodCraftWorld.invoke(craftWorld);
				Object blockPosition = BlockPositionClass.getConstructor(new Class<?>[]{int.class, int.class, int.class}).newInstance(spawner.getX(), spawner.getY(), spawner.getZ());
				Object tileEntity = getTileEntityMethodWorld.invoke(world, blockPosition);
				Object mobSpawner = TileEntityMobSpawnerClass.cast(tileEntity);
				
				Object spawnerTag = NBTTagCompoundClass.newInstance();
				saveMethodTileEntityMobSpawner.invoke(mobSpawner, spawnerTag);
				removeMethodNBTTagCompound.invoke(spawnerTag, "SpawnPotentials");

				Object itemTag = NBTTagCompoundClass.newInstance();
				setShortMethodNBTTagCompound.invoke(itemTag, "Health", (short) 5);
				setShortMethodNBTTagCompound.invoke(itemTag, "Age", (short) 0);

				Object handleitemStack = asNMSCopyCraftItemStack.invoke(CraftItemStackClass.getClass(), displayItem);

				Object itemStackTag = NBTTagCompoundClass.newInstance();

				saveItemStack.invoke(handleitemStack, itemStackTag);
				setByteMethodNBTTagCompound.invoke(itemStackTag, "Count", (byte)1);
				setMethodNBTTagCompound.invoke(itemTag, "Item", itemStackTag);
				setStringMethodNBTTagCompound.invoke(itemTag, "id", "item");
				setMethodNBTTagCompound.invoke(spawnerTag, "SpawnData", itemTag);
				loadMethodTileEntityMobSpawner.invoke(mobSpawner, spawnerTag);
			}
		}catch (Exception e) {
			Debugger.runReport(e);
		}
	}
}
